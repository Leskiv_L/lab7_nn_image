import nn_cat as nn
import numpy as np

x1 = nn.read_photo('cat.jpg')
x2 = nn.read_photo('dog.jpg')
x3 = nn.read_photo('bird.jpg')
x4 = nn.read_photo('cat1.jpg')
l = [x1.size, x2.size, x3.size, x4.size]
num_x = max(l)
x1 = np.append(x1, [0 for j in range(num_x - l[0])])
x2 = np.append(x2, [0 for j in range(num_x - l[1])])
x3 = np.append(x3, [0 for j in range(num_x - l[2])])
x4 = np.append(x4, [0 for j in range(num_x - l[3])])


t_x = np.array([x1, x2, x3, x4]).T
t_y = np.array([[1, 0, 0, 1]])

epoch1 = 100
epoch2 = 500
epoch3 = 1000
epoch4 = 4000

n = nn.NeuralNetwork()


print('\t\tОчікувані результати\n [[1, 0, 0, 1]]')

n.train(t_x, t_y, epoch1)
print('\t\tРезультат для epoch = 100\n', n.think(t_x))

n.train(t_x, t_y, epoch2)
print('\t\tРезультат для epoch = 500\n', n.think(t_x))

n.train(t_x, t_y, epoch3)
print('\t\tРезультат для epoch = 1000\n', n.think(t_x))

n.train(t_x, t_y, epoch4)
print('\t\tРезультат для epoch = 4000\n', n.think(t_x))
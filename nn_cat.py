import numpy as np
from PIL import Image
from matplotlib import pyplot


def read_photo(photo):
    matrix = np.array(Image.open(photo))
    return matrix.flatten()


class NeuralNetwork:

    def __init__(self):
        np.random.seed(1)
        self.synaptic_weights = np.zeros((921600, 1))
        self.b = 0
        self.dj = [0]

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def think(self, inputs):
        inputs = inputs.astype(float)
        z = np.dot(self.synaptic_weights.T, inputs) + self.b
        a = self.sigmoid(z)
        return a

    def train(self, x, y, epoch):
        J = [0]
        eps = 0.0001
        m = len(x)
        alpha = 0.1
        for i in range(epoch):
            a = self.think(x)
            J.append(-(y * np.log(a) + (1 - y) * np.log(1 - a)))

            dz = a - y

            dw = (1 / m) * np.dot(x, dz.T)
            db = (1 / m) * np.sum(dz)
            self.synaptic_weights -= alpha * dw
            self.b -= alpha * db

            self.dj = ((1 / m) * J[i] - J[i - 1])
            # condition
            if np.all(abs(self.dj) <= eps):
                break

    def plot_cost_function(self):
        pyplot.plot(self.dj[0])
        pyplot.xlabel("item")
        pyplot.ylabel("J")
        pyplot.title("COST FUNCTION")
        pyplot.legend(['J = -(1/m) * (y * np.log(a) + (1 - y) * np.log(1 - a))'])
        pyplot.grid(True)
        pyplot.show()


if __name__ == '__main__':
    x1 = read_photo('cat.jpg')
    x2 = read_photo('dog.jpg')
    x3 = read_photo('bird.jpg')
    x4 = read_photo('cat1.jpg')
    x5 = read_photo('dogsmall.jpg')
    x6 = read_photo('cat2.jpg')

    l = [x1.size, x2.size, x3.size, x4.size, x5.size, x6.size]
    num_x = max(l)
    print('Кількість елементів х ', num_x)

    x1 = np.append(x1, [0 for j in range(num_x - l[0])])
    x2 = np.append(x2, [0 for j in range(num_x - l[1])])
    x3 = np.append(x3, [0 for j in range(num_x - l[2])])
    x4 = np.append(x4, [0 for j in range(num_x - l[3])])
    x5 = np.append(x5, [0 for j in range(num_x - l[4])])
    x6 = np.append(x6, [0 for j in range(num_x - l[5])])
    x7 = np.append(x7, [0 for j in range(num_x - l[6])])
    x8 = np.append(x8, [0 for j in range(num_x - l[7])])
    
    t_x = np.array([x1, x2, x3, x4, x5, x6, x7, x8]).T
    t_y = np.array([[1, 0, 0, 1, 0, 1, 1, 1]])
    print('shape of x', t_x.T.shape)
    print(len(t_x))

    neural_network = NeuralNetwork()
    print("Початкові ваги: ")
    print(neural_network.synaptic_weights)

    neural_network.train(t_x, t_y, 500)
    print("Кінцеві ваги після тренування: ")
    print(neural_network.synaptic_weights)

    a = neural_network.think(t_x)
    print("Результат навчання: \n", a)

    result = []
    for i in a:
        for j in i:
            if np.all(j > 0.5):
                result.append(1)
            else:
                result.append(0)
    print("Округлені результати:\n", result)
    print('Функція втрат:\n', neural_network.dj)

    neural_network.plot_cost_function()

    # TESTING
    print('TESTING THE NEURAL NETWORK')
    res1 = neural_network.think(x5)
    res2 = neural_network.think(x6)

    if res1 < 0.5:
        print('test 1 passed\n it do not a cat')
    else:
        print('test 1 bad\n it is a cat')

    if res2 > 0.5:
        print('test 2 passed\n it is a cat')
    else:
        print('test 2 error\n it do not a cat')